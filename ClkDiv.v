module ClkDiv(
    input clk_in, // Input clock at 8MHz
    output clk_out // Output clock at 1Hz
);

	reg [22:0] counter;
	reg clk_out_reg;

	assign clk_out = clk_out_reg;

	always @ (posedge clk_in)
	begin
		 counter = counter + 1;
		 
		 if (counter == 2500000) begin
			  clk_out_reg = !clk_out_reg;
			  counter = 25'h0;
		 end
	end

	initial begin
		 clk_out_reg = 0;
		 counter = 22'h0;
	end

endmodule
