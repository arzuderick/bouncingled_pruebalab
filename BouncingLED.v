module BouncingLED(
	input clk,
	input reset,
	output reg[7:0] LED
);

wire clk50;

ClkDiv clkD(clk, clk50);

reg [3:0]current_state;
reg [3:0]next_state;

always @(current_state)
begin
	case(current_state)
		4'b0000:
		begin
			next_state = 4'b0001;
			LED = 8'b00000000;
		end
		4'b0001:
		begin
			next_state = 4'b0010;
			LED = 8'b00000001;
		end
		4'b0010:
		begin
			next_state = 4'b0011;
			LED = 8'b00000010;
		end
		4'b0011:
		begin
			next_state = 4'b0100;
			LED = 8'b00000100;
		end
		4'b0100:
		begin
			next_state = 4'b0101;
			LED = 8'b00001000;
		end
		4'b0101:
		begin
			next_state = 4'b0110;
			LED = 8'b00010000;
		end
		4'b0110:
		begin
			next_state = 4'b0111;
			LED = 8'b00100000;
		end
		4'b0111:
		begin
			next_state = 4'b1000;
			LED = 8'b01000000;
		end
		4'b1000:
		begin
			next_state = 4'b1001;
			LED = 8'b10000000;
		end
		4'b1001:
		begin
			next_state = 4'b1010;
			LED = 8'b01000000;
		end
		4'b1010:
		begin
			next_state = 4'b1011;
			LED = 8'b00100000;
		end
		4'b1011:
		begin
			next_state = 4'b1100;
			LED = 8'b00010000;
		end
		4'b1100:
		begin
			next_state = 4'b1101;
			LED = 8'b00001000;
		end
		4'b1101:
		begin
			next_state = 4'b1110;
			LED = 8'b00000100;
		end
		4'b1110:
		begin
			next_state = 4'b1111;
			LED = 8'b00000010;
		end
		4'b1111:
		begin
			next_state = 4'b0000;
			LED = 8'b00000001;
		end
		default
			next_state = 4'bxxxx;
	endcase
end

always @(posedge clk50)
	begin
		if(reset == 0)
			current_state = next_state;
		else
			current_state = 4'b0000;
	end
endmodule
